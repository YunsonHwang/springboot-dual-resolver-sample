<%--
  Created by IntelliJ IDEA.
  User: walter
  Date: 2019-05-19
  Time: 15:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<title>Title</title>
</head>
<body>
<div>
	Language List (JSP)
	<ul>
		<c:forEach var="lang" items="${list}">
			<li>
				<a href="/language/${lang.id}">${lang.name}</a> - ${lang.owner}
			</li>
		</c:forEach>
	</ul>
</div>

</body>
</html>
