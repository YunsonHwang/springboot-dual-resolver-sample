package com.walter.project.service;

import com.walter.project.entity.Language;

import java.util.List;
import java.util.Optional;

public interface LanguageService {
	List<Language> getList();
	Optional<Language> save(Language language);
}
