package com.walter.project.service;

import com.walter.project.entity.Language;
import com.walter.project.repository.LanguageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LanguageServiceImpl implements LanguageService {

	final private LanguageRepository languageRepository;

	@Override
	public List<Language> getList() {
		return languageRepository.findAll();
	}

	@Override
	public Optional<Language> save(Language language) {
		return Optional.ofNullable(languageRepository.save(language));
	}
}
