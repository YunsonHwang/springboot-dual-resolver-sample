package com.walter.project.config;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

import java.net.MalformedURLException;
import java.util.Locale;

@RequiredArgsConstructor
public class CustomThymeleafViewResolver extends ThymeleafViewResolver {

	public static final String RESOURCE_PREFIX_CLASSPATH = "classpath:";
	public static final String RESOURCE_PREFIX_FILE = "file:";

	final private SpringResourceTemplateResolver springResourceTemplateResolver;

	@Setter
	private String prefix;

	@Setter
	private String suffix;

	@Override
	protected boolean canHandle(String viewName, Locale locale) {
		boolean isExistView = isExistView(viewName);
		if (isExistView) {
			return super.canHandle(viewName, locale);
		}
		return isExistView;
	}

	protected boolean isExistView(String viewName) {
		String viewPath =
				StringUtils.defaultString(this.prefix, springResourceTemplateResolver.getPrefix()) +
				viewName +
				StringUtils.defaultString(this.suffix, springResourceTemplateResolver.getSuffix());

		Resource resource = null;
		if (viewPath.startsWith(RESOURCE_PREFIX_CLASSPATH)) {
			resource = new ClassPathResource(StringUtils.removeStart(viewPath, RESOURCE_PREFIX_CLASSPATH));
		} else if (viewPath.startsWith(RESOURCE_PREFIX_FILE)) {
			resource = new FileSystemResource(StringUtils.removeStart(viewPath, RESOURCE_PREFIX_FILE));
		} else {
			try {
				resource = new UrlResource(viewPath);
			} catch (MalformedURLException e) {
				return false;
			}
		}

		if (!resource.exists()) {
			return false;
		}
		return true;
	}
}
