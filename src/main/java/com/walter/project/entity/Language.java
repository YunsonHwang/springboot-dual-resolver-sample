package com.walter.project.entity;

import lombok.Data;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

@Data
@Entity
public class Language extends AbstractPersistable<Long> {
	private String name;
	private String owner;

	@Column(name = "is_object_oriented")
	private boolean isObjectOriented;

	private String description;

	@Column(name = "reg_date")
	private Date regDate;
}
