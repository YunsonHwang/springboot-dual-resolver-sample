package com.walter.project.controller;

import com.walter.project.entity.Language;
import com.walter.project.service.LanguageService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Mono;

import java.util.Date;

@Controller
@RequiredArgsConstructor
public class LanguageController {

	final private LanguageService languageService;

	@GetMapping(value = "/language")
	public Mono<String> index(Model model) {
		model.addAttribute("list", languageService.getList());
		return Mono.just("language/index");
	}

	@PostMapping(value = "/language")
	@ResponseBody
	public ResponseEntity<?> save(@ModelAttribute Language language) {
		language.setRegDate(new Date());
		return ResponseEntity
				.ok()
				.body(languageService.save(language));
	}

}
