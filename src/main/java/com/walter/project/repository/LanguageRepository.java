package com.walter.project.repository;

import com.walter.project.entity.Language;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LanguageRepository extends JpaRepository<Language, Long> {
	Optional<Language> findById(Long id);
}
