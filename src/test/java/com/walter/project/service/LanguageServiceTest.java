package com.walter.project.service;

import com.walter.project.entity.Language;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class LanguageServiceTest {

	@Autowired
	private LanguageService languageService;

	@Test
	public void getListTest() {
		assertThat(languageService.getList()).isNotNull();
	}

	@Test
	public void saveTest() {
		Language language = new Language();
		language.setName("Java");
		language.setDescription("객체지향 프로그래밍 언어. JVM기반으로 동작한다.");
		language.setOwner("Oracle");
		language.setObjectOriented(true);
		language.setRegDate(new Date());

		assertThat(languageService.save(language).get().getId()).isNotNull();
	}

}
